
#### Getting Started
Welcome to Sasto Tickets Android SDK documentation.

Sasto Tickets SDK enables customers to book different types of flights from the app.

<br>
#### Requirements
> Min SDK: 19

<br>
#### Add SDK to your project

The .aar file must be added to libs folder of your projects app module (Your Project -> app -> libs ). After that add the following code in your project-level build.gradle:

    
    buildscript {
    repositories {
        google()
        mavenCentral()
        flatDir {
            dirs 'libs'
        }
    }

On app-level build.gradle, add the following implementation on dependencies

    dependencies {
        implementation fileTree(include: ['*.jar', '*.aar'], dir: 'libs') 
        implementation 'com.google.code.gson:gson:2.8.9'
        implementation 'com.facebook.shimmer:shimmer:0.5.0'
    }


That&#39;s it. Sync gradle project and you&#39;re good to go. 🎉


<br>
#### Starting SDK in Kotlin

Call the following function wherever you want to start the Sasto Tickets SDK

    SastoTicketsConfiguration().initilizeSDK(this, "email", "password",::callBack)

Define callback function as:

    fun callBack(response:String?,errorMessage:String?){
        //Do things according to response we get
        if (response!=null) {
            Log.e("sastoticketdebug",response)
        } else {
            Log.e("sastoticketdebugerror",errorMessage.toString())
        }
    }
    

<br>
#### Starting SDK in Java
Call the following function whereever you want to start the Sasto Tickets SDK

    new SastoTicketsConfiguration().initilizeSDK(this, "email",  "password",
                (response, errorMessage) -> {
                    MainActivity.this.callBack(response, errorMessage);
                    return Unit.INSTANCE;
                }
        );

Define callback function as:
    
    public final void callBack(@Nullable String response, @Nullable String errorMessage) {
        if (response != null) {
            Log.e("sastoticketdebug", response);
        } else {
            Log.e("sastoticketdebugerror", String.valueOf(errorMessage));
        }
        findViewById(R.id.progressbar).setVisibility(View.GONE);
    }

<br>
#### Credentials & URLs
Each client will have different credentials as provided by the Sasto Tickets team.
